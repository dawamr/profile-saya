<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Kasir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('transaksi', function (Blueprint $table) {
           $table->increments('id_trx');
           $table->string('namaBarang');
           $table->integer('hargaSatuan');
           $table->integer('jumlahPembelian');
           $table->integer('diskon');
           $table->integer('potonganKhusus');
           $table->integer('totalHarga');
           $table->string('namaPembeli');
           $table->string('metodeBayar');
           $table->integer('totalBayar');
           $table->integer('kembalian');
           $table->date('tglPembelian');
           $table->timestamps();
      });
      Schema::create('barang', function (Blueprint $table) {
           $table->increments('id_barang');
           $table->string('kodeBarang')->unique();
           $table->string('namaBarang')->nullable();
           $table->integer('stock')->nullable();
           $table->integer('harga')->nullable();
           $table->integer('hargaJual')->nullable();
           $table->integer('diskon')->nullable();
      });
      Schema::create('hutang', function (Blueprint $table) {
         $table->increments('id_hutang');
        $table->string('identitasBON');
        $table->string('namaBON')->nullable();
        $table->integer('hutangBON')->nullable();
        $table->date('tanggalBON')->nullable();
        $table->string('status')->nullable();
      });
    }

    // kodeBarang, namaBarang, stock, hargaJual, diskon

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
