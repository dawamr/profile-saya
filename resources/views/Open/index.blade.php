@extends('layouts.master')
@section('content')
<section id="who">
<div class="home-header-divider"></div>
    <div class="container">
      <div class="title-section">
          <h2>Dawam raja</h2>
          <hr>
      </div>
        <div class="row no-gutters">
          <div class="col-md-3 col-sm-6">
              <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                  <p class="h4">Biarkan Hati Berkata</p>
                </div>
              </div>
              <div class="img-fluid">
              <img class="" src="/asset/img/portfolio/row1.png" onclick="Row1()" alt="">
              </div>
              <script type="text/javascript">
                function Row1() {
                  window.open('http://facebook.com/BiarkanHatiBerkata.Official/')
                }
              </script>
          </div>
          <div class="col-md-3 col-sm-6">
              <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                  <p class="h4">Cooming Soon</p>
                </div>
              </div>
              <div class="img-fluid">
              <img class="img-fluid" src="/asset/img/portfolio/row2.png" alt="">
              </div>
          </div>
          <div class="col-md-3 col-sm-6">
              <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                  <p class="h4">Cooming Soon</p>
                </div>
              </div>
              <div class="img-fluid">
              <img class="img-fluid" src="/asset/img/portfolio/row3.png" alt="">
              </div>
          </div>
          <div class="col-md-3 col-sm-6">
              <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                  <p class="h4">Coomig Soon</p>
                </div>
              </div>
              <div class="img-fluid">
              <img class="img-fluid" src="/asset/img/portfolio/row4.png" alt="">
              </div>
          </div>

    </div>
</section>
    <!-- About Section -->
    <section id="about">
    <div class="home-header-divider"></div>
        <div class="container">
            <div class="title-section">
                <h2>About</h2>
                <hr>
            </div>

            <div class="info">
                <h3>Fun Fact About Me :</h3>
                <p>A boy who loved Coding more than he likes you, my favorite number is 09, I'm a huge fan of Muhammad SAW, I constantly typing syntax like it's my life, I didn't like to take a photo or selfie, I knew what I wanted to do at the age of 14, my favorite food is fried rice(you know? that's favorit indonesian foods ). Now,I am studying at SMK N 8 Semarang class 11.</p>
            </div>
            <div class="img-profile">
                <img src="https://document-export.canva.com/DACxQb-i3eo/96/preview/0001-920246881.png" class="img-circle" alt="">
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-7">
                    <center>
                        <h3>Contact Info : </h3></center>
                    <div class="contact-box">
                        <p></p>
                        <div class="row contact-box-icon">
                            <div class="col-xs-12 col-sm-6">
                                <div class="contact-icon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="contact-info">
                                    <h4>Name :</h4>
                                    <p>Dawam Rajaa H</p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="contact-icon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="contact-info">
                                    <h4>Email :</h4>
                                    <p>akunnyadawam9@gmail.com</p>
                                </div>
                            </div>
                        </div>

                        <div class="row contact-box-icon">
                            <div class="col-xs-12 col-sm-6">
                                <div class="contact-icon">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="contact-info">
                                    <h4>Location :</h4>
                                    <p>Semarang, Indonesia</p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="contact-icon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <div class="contact-info">
                                    <h4>WhatsApp :</h4>
                                    <p>+62 857-8347-6917</p>
                                </div>
                            </div>
                        </div>

                        <div class="row contact-box-icon">
                            <div class="col-xs-12 col-sm-6">
                                <div class="contact-icon">
                                    <i class="fa fa-birthday-cake"></i>
                                </div>
                                <div class="contact-info">
                                    <h4>Birth :</h4>
                                    <p>14 December 2000</p>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="contact-icon">
                                    <i class="fa fa-flag"></i>
                                </div>
                                <div class="contact-info">
                                    <h4>Nation :</h4>
                                    <p>Indonesian</p>
                                </div>
                            </div>
                        </div>

                        <div class="row signature">
                            <h3 style="font-family: 'Yellowtail', cursive;">Dawam Raja</h3>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-5">
                    <center>
                        <h3>Skills : </h3>
                    </center>
                    <div class="skills">
                        <p>This is some my skills, hopefully I can growing up and be a good Web Developer </p>
                        <span>HTML5</span>
                        <div class="skillbar" data-percent="91%">
                            <div class="skillbar-bar" style="background: #e67e22;"></div>
                            <div class="skill-bar-percent">80%</div>
                        </div>

                        <span>CSS3</span>
                        <div class="skillbar" data-percent="87%">
                            <div class="skillbar-bar" style="background: #3498db;"></div>
                            <div class="skill-bar-percent">80%</div>
                        </div>

                        <span>jQuery</span>
                        <div class="skillbar" data-percent="30%">
                            <div class="skillbar-bar" style="background: #2c3e50;"></div>
                            <div class="skill-bar-percent">45%</div>
                        </div>

                        <span>Laravel</span>
                        <div class="skillbar" data-percent="60%">
                            <div class="skillbar-bar" style="background: #5a68a5;"></div>
                            <div class="skill-bar-percent">60%</div>
                        </div>

                        <span>Visual Basic .Net</span>
                        <div class="skillbar" data-percent="75%">
                            <div class="skillbar-bar" style="background: #4db6ad;"></div>
                            <div class="skill-bar-percent">60%</div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>

    <!-- Interest Section -->
    <!--
    <section id="interest">
        <div class="container"><br><br>
            <div class="title-section">
                <h2>Interest</h2>
                <hr>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-3 interest" style="margin-top: 25px;">
                    <div class="service-box">
                        <div class="icon-interest">
                            <i class="fa fa-gamepad"></i>
                        </div>
                        <div class="info-interest">
                            <h3>Game</h3>
                            <p>I'm really love game either on a handphone or on a computer, but it when i was a kid. Now I'm playing game too, but not often because you know my computer spec isn't enough for game now(sad). When i was 10 years old, i always play PES game. Until now i love PES although i don't play that game anymore.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 interest" style="margin-top: 25px;">
                    <div class="service-box">
                        <div class="icon-interest">
                            <i class="fa fa-code"></i>
                        </div>
                        <div class="info-interest">
                            <h3>Web Programming</h3>
                            <p>When the first time i am join to Vocational Highschool, I know about Web Programming. First, I don't care about Web Programming subject because i don't like it. But, after a long time learn about that subject suddenly I really love into Web Programming.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 interest" style="margin-top: 25px;">
                    <div class="service-box">
                        <div class="icon-interest">
                            <i class="fa fa-desktop"></i>
                        </div>
                        <div class="info-interest">
                            <h3>Computer</h3>
                            <p>When i was a kid, i'm already pretty good at it because my mother is a computer teacher. One day my parent bought me a computer and i was really so exiceted. With that computer, i became understand about computer. I just wanna say thanks to my mother about everything you give to me :) </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 interest" style="margin-top: 25px;">
                    <div class="service-box">
                        <div class="icon-interest">
                            <i class="fa fa-headphones"></i>
                        </div>
                        <div class="info-interest">
                            <h3>Music</h3>
                            <p>Maybe all the people really love music. I can't sing but i love music, i just a listener and enjoying a music. My favorite band is Coldplay and Linkin Park. I really love both of them! because they always created a good songs. The songs of my favorite is "Coldplay - Fix You" and "Linkin Park - Numb".</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
 -->
    <!-- Portfolio Section -->
    <!--
    <section id="portfolio">
        <div class="container"><br><br>
            <div class="title-section">
                <h2>Portfolio</h2>
                <hr>
            </div>

            <div class="portfolioFilter" style="margin-bottom: 30px;">
                <center>
                    <a href="#" data-filter="*" class="current">All Categories</a>
                    <a href="#" data-filter=".flatdesign">Flat Design</a>
                    <a href="#" data-filter=".flaticon">Flat Icon</a>
                    <a href="#" data-filter=".lowpoly">Lowpoly</a>
                    <a href="#" data-filter=".typography">Typography</a>
                </center>
            </div>


            <div class="row port-container">
                <div class="col-md-4 col-xs-12 port flatdesign flaticon" style="padding-bottom: 5%;">
                    <img src="img/portofolio/flat-icon.jpg">
                    <div class="overlay">
                        <div class="lines">
                            <a class="zoom" href="img/portofolio/flat-icon.jpg">
                                <span class="ti-zoom-in"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12 port flatdesign" style="padding-bottom: 5%;">
                    <img src="img/portofolio/flatdesign.jpg">
                    <div class="overlay">
                        <div class="lines">
                            <a class="zoom" href="img/portofolio/flatdesign.jpg">
                                <span class="ti-zoom-in"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12 port lowpoly" style="padding-bottom: 5%;">
                    <img src="img/portofolio/lowpoly.jpg">
                    <div class="overlay">
                        <div class="lines">
                            <a class="zoom" href="img/portofolio/lowpoly.jpg">
                                <span class="ti-zoom-in"></span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12 port food typography" style="padding-bottom: 5%;">
                    <img src="img/portofolio/fanatik.jpg">
                    <div class="overlay">
                        <div class="lines">
                            <a class="zoom" href="img/portofolio/fanatik.jpg">
                                <span class="ti-zoom-in"></span>
                            </a>
                        </div>
                    </div>
                </div>


                <div class="col-md-4 col-xs-12 port lowpoly" style="padding-bottom: 5%;">
                    <img src="img/portofolio/dory-lowpoly.jpg">
                    <div class="overlay">
                        <div class="lines">
                            <a class="zoom" href="img/portofolio/dory-lowpoly.jpg">
                                <span class="ti-zoom-in"></span>
                            </a>
                        </div>
                    </div>
                </div>


                <div class="col-md-4 col-xs-12 port flatdesign flaticon" style="padding-bottom: 5%;">
                    <img src="img/portofolio/artboard.png">
                    <div class="overlay">
                        <div class="lines">
                            <a class="zoom" href="img/portofolio/artboard.jpg">
                                <span class="ti-zoom-in"></span>
                            </a>
                        </div>
                    </div>
                </div>


            </div>
            <center><a href="portfolio-single.html" target="_blank" class="btn-read-more">View All</a></center>
        </div>
    </section>
 -->
    <!-- Blog Section -->
    <!-- <section id="blog">
        <div class="container">
            <div class="title-section">
                <h2>Blog</h2>
                <hr>
            </div>

            <div class="row blog-box">
                <div class="col-md-4 col-xs-12">
                    <div class="blog-content">
                        <figure><img src="img/blog/code.png"></figure>
                        <div class="blog-date">
                            <h4>15</h4> May 2017
                        </div>
                        <div class="blog-text">
                            <h3>6 Situs Belajar Web Programming</h3>
                            <div class="posted">
                                <i class="fa fa-user-circle-o"></i><span>Posted by mmuqiit</span> <i class="fa fa-folder"></i><span>Article</span>
                            </div>
                            <p>Halo! Apakah kalian tertarik untuk menjadi Web Programmer? Kalau iya maka saya mempunyai referensi untuk belajar Web Programming. Web Programming memungkinkan kamu untuk bisa membuat website sendiri. Bahasa pemrograman yang digunakan untuk membuat website meliputi HTML, CSS ...</p>
                            <center><a href="https://muqiitf.wordpress.com/2017/05/15/6-situs-belajar-web-programming/" target="_blank" class="btn-read-more">Read More</a></center>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12">
                    <div class="blog-content">
                        <figure><img src="img/blog/sublime.jpg"></figure>
                        <div class="blog-date">
                            <h4>24</h4> March 2017
                        </div>
                        <div class="blog-text">
                            <h3>Teks Editor Terbaik</h3>
                            <div class="posted">
                                <i class="fa fa-user-circle-o"></i><span>Posted by mmuqiit</span> <i class="fa fa-folder"></i><span>Article</span>
                            </div>
                            <p>Halo! Kali ini saya akan membahas tentang teks editor terbaik untuk kamu yang suka sekali coding atau programming. Teks editor biasanya hanya mempunyai fungsi untuk menulis kode program atau coding dan tanpa disertai dengan compiler. Teks editor sering digunakan oleh seorang web developer membuat sebuah halaman website. Ada banyak se ...</p>
                            <center><a href="https://muqiitf.wordpress.com/2017/03/23/teks-editor-terbaik-untuk-kamu-yang-suka-programming/" target="_blank" class="btn-read-more">Read More</a></center>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12">
                    <div class="blog-content">
                        <figure><img src="img/blog/linux.jpg"></figure>
                        <div class="blog-date">
                            <h4>19</h4> March 2017
                        </div>
                        <div class="blog-text">
                            <h3>Distro Linux Terbaik</h3>
                            <div class="posted">
                                <i class="fa fa-user-circle-o"></i><span>Posted by Doe</span> <i class="fa fa-folder"></i><span>Web Design</span>
                            </div>
                            <p>Apa itu Linux? Linux adalah sistem operasi sistem operasi berbasis UNIX. Linux sendiri diambil dari nama pembuatnya yaitu Linus Torvalds. Linux merupakan salah satu contoh hasil pengembangan perangkat lunak bebas dan open source. Distro Linux (singkatan dari distribusi Linux) adalah sebutan untuk sistem operasi komputer dan aplikasinya, merupakan ...</p>
                            <center><a href="https://muqiitf.wordpress.com/2017/03/19/distro-linux-terbaik/" target="_blank" class="btn-read-more">Read More</a></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="title-section">
                <h2>Contact</h2>
                <hr>
            </div>

            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <div class="social">
                        <h3>GET IN TOUCH</h3>
                        <div class="contact-item">
                            <ul>
                              <li>
                                <a href="https://facebook.com/m.dawamraja" target="_blank" style="color: #9ea8b6;"><p> <span style="color:black" class="fab fa-facebook"></span>&nbsp m.DawamRaja</p></a>
                            </li>
                                <li>
                                    <a href="https://twitter.com/m_dawamraja" target="_blank" style="color: #9ea8b6;"><p><span style="color:black" class="fab fa-twitter"></span>&nbsp @m_dawamraja</p></a>
                                </li>
                                <li>
                                    <a href="#" target="_blank" style="color: #9ea8b6;"><p><span style="color:black" class="fab fa-telegram"></span>&nbsp Waiting..</p></a>
                                </li>
                                <li>
                                    <a href="https://plus.google.com/118000452028892886589" target="_blank" style="color: #9ea8b6;"><span style="color:black" class="fab fa-google-plus"></span>&nbsp Dawam Raja </a>
                                </li>
                                <li>
                                    <a href="https://instagram.com/dawamraja" target="_blank" style="color: #9ea8b6;"><p><span style="color:black" class="fab fa-instagram"></span>&nbsp @dawamraja</p></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <form class="col-md-8 col-xs-12" action="/send" method="post">
                  {{csrf_field()}}
                    <center>

                        <h3>Message To Me</h3></center>
                    <div class="col-md-6 placeholder">
                        <input type="name" name="name" id="nama" placeholder="Name" class="input" required>
                    </div>
                    <div class="col-md-6 placeholder">
                        <input type="email" name="email" placeholder="Email" class="input" required>
                    </div>
                    <div class="col-md-12">
                        <textarea rows=10 cols="100" name="message" id="pesan" placeholder="Comment" class="textarea" required></textarea>
                    </div>
                    <input type="submit" name="submit" onclick="" value="Submit" class="btn-submit">
                </form>

            </div>

            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.161404764728!2d110.41767843067646!3d-6.990261821161281!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x552f32390f4f9eb4!2sSMKN+8+Semarang!5e0!3m2!1sen!2sid!4v1520361698157" width="600" height="450" frameborder="0" style="border:0;height:400px;width:100%;margin-top:40px;" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>

@endsection
@Section('js')




@endsection
