
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Dawam Raja - LARAVELBuild</title>

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="/asset/bootstrap.css" media="screen,projection" />
    <style media="screen">
      .hover:hover{
        color:#ffffff !important;
      }
    </style>
    <!-- jQuery -->
    <script type="text/javascript" src="/asset/jquery.min.js"></script>

    <!-- Fonts Google -->
    <link href="https://fonts.googleapis.com/css?family=Maven+Pro:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <!-- Stylesheet -->
    <link rel="stylesheet" type="text/css" href="/asset/stylesheet.css">
    <!-- Custom styles for this template -->
    <!-- <link href="/asset/freelancer.min.css" rel="stylesheet"> -->
    <!-- Icons -->
    <!-- <link rel="stylesheet" type="text/css" href="/asset/font-awesome.min.css"> -->
    <link rel="stylesheet" type="text/css" href="/asset/themify-icons.css">

    <!-- Javascript Framework -->
    <script src="/asset/bootstrap.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

    <!-- <script src="/asset/jquery.magnific-popup.js"></script> -->
    <link rel="stylesheet" href="/asset/magnific-popup.css">
    <script type="text/javascript" src="/asset/typed.js"></script>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="80">
    <!-- Preloading Pages -->
    <!-- <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div> -->

    <!-- Navigation -->
    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand hover" href="#home">DawamRaja</a>
            </div>
            <div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li><a href="#home">HOME</a></li>
                        <li><a href="#who">PHOTOS</a></li>
                        <li><a href="#about">ABOUT</a></li>
                        <li><a href="#contact">CONTACT</a></li>
                        <!-- <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">PAGES <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="404-page.html" target="_blank">404 Page</a></li>
                                <li><a href="portfolio-single.html" target="_blank">Single Portfolio</a></li>
                            </ul>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
    </nav>

    <!-- Home Section -->
    <section id="home">
        <div id="particles-js">
        <div class="welcome-particles">
            <div class="social-home">
                <a href="https://facebook.com/m.dawamraja" target="_blank"><i class="fab fa-facebook fa-2x"></i></a> &nbsp&nbsp&nbsp
                <a href="https://dawamraja.blogspot.com" target="_blank"><i class="fab fa-blogger fa-2x"></i></a>&nbsp&nbsp&nbsp
                <a href="https://github.com/dawamr" target="_blank"><i class="fab fa-github fa-2x"></i></a>
            </div><br><br><br>
            <div class="hello"><span class="typed"></span></div><br>
            <a href="#contact"><span class="hire-me">Hire Me</span></a>
            <a href="/asset/cv.pdf"><span class="cv" >Download CV</span></a>
        </div>

        <div class="scroll-down">
            <a href="#about"><i class="fa fa-chevron-down"></i></a>
        </div>
        </div>
    </section>
    @yield('content')

    <!-- Footer -->
    <footer>
        &copy; 2018 Dawam Raja | All rights reseverd.
    </footer>
@yield('js')
    <!-- Framework JS -->
    <!-- Plugin JavaScript -->
    <script src="/asset/jquery.easing.min.js"></script>
    <script src="/asset/jquery.magnific-popup.min.js"></script>
    <!-- <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script> -->
    <!-- <script src="/asset/stats.js"></script> -->
    <script type="text/javascript" src="/asset/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="/asset/isotope.js"></script>
    <script type="text/javascript" src="/asset/custom.js"></script>
    <!-- scripts -->
    <!-- <script src="/asset/particles.js"></script>
    <script src="/asset/app.js"></script> -->
    <!-- Custom scripts for this template -->
    <script src="/asset/freelancer.min.js"></script>
    <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <!-- stats.js lib -->
    <!-- <script src="https://threejs.org/examples/js/libs/stats.min.js"></script> -->
    <script type="text/javascript">
    particlesJS("particles-js", {
      "particles": {
        "number": {
          "value": 137,
          "density": {
            "enable": true,
            "value_area": 800
          }
        },
        "color": {
          "value": "#ffffff"
        },
        "shape": {
          "type": "polygon",
          "stroke": {
            "width": 0,
            "color": "#000000"
          },
          "polygon": {
            "nb_sides": 5
          },
          "image": {
            "src": "img/github.svg",
            "width": 100,
            "height": 100
          }
        },
        "opacity": {
          "value": 0.5,
          "random": false,
          "anim": {
            "enable": false,
            "speed": 1,
            "opacity_min": 0.1,
            "sync": false
          }
        },
        "size": {
          "value": 3,
          "random": true,
          "anim": {
            "enable": false,
            "speed": 40,
            "size_min": 0.1,
            "sync": false
          }
        },
        "line_linked": {
          "enable": true,
          "distance": 150,
          "color": "#ffffff",
          "opacity": 0.4,
          "width": 1
        },
        "move": {
          "enable": true,
          "speed": 11.22388442605866,
          "direction": "none",
          "random": true,
          "straight": false,
          "out_mode": "bounce",
          "bounce": false,
          "attract": {
            "enable": false,
            "rotateX": 600,
            "rotateY": 1200
          }
        }
      },
      "interactivity": {
        "detect_on": "canvas",
        "events": {
          "onhover": {
            "enable": true,
            "mode": "repulse"
          },
          "onclick": {
            "enable": true,
            "mode": "push"
          },
          "resize": true
        },
        "modes": {
          "grab": {
            "distance": 400,
            "line_linked": {
              "opacity": 1
            }
          },
          "bubble": {
            "distance": 400,
            "size": 40,
            "duration": 2,
            "opacity": 8,
            "speed": 3
          },
          "repulse": {
            "distance": 200,
            "duration": 0.4
          },
          "push": {
            "particles_nb": 4
          },
          "remove": {
            "particles_nb": 2
          }
        }
      },
      "retina_detect": true
    });
    var count_particles, stats, update;
    stats = new Stats;
    stats.setMode(0);
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.left = '0px';
    stats.domElement.style.top = '0px';
    document.body.appendChild(stats.domElement);
    count_particles = document.querySelector('.js-count-particles');
    update = function() {
      stats.begin();
      stats.end();
      if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
        count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
      }
      requestAnimationFrame(update);
    };
    requestAnimationFrame(update);;
    </script>

</body>

</html>
