<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use Illuminate\Database\DatabaseServiceProvider;
use Illuminate\Support\Facades\Input;
class ProfileController extends Controller
{
    public function index(Request $request){

      return view('Open/index');
    }

    public function send(Request $request){
      $name = $request->name;
      $email = $request->email;
      $message = $request->message;


      $simpan = new \App\Message;
      $simpan->name = $name;
      $simpan->email = $email;
      $simpan->message =$message;
      $simpan->save();


      return redirect('direct')->with('name', $name);
    }
}
